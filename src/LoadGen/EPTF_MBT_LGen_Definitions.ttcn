///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:     EPTF_MBT_LGen_Definitions.ttcn
//  Rev:      <RnXnn>
//  Prodnr:   CNL 113 659
//  Updated:  2010-10-01
//  Contact:  http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////
//  Module: EPTF_MBT_LGen_Definitions
//
//  Purpose:
//    Type definitions for the MBT Applib
//
//  Detailed comments:
//    The MBT applib provides a simple FSM that can catch the reported events from the applibs
//    and can execute all desired test steps registered by the applibs. The behavior of the 
//    FSM can be controlled via functions in the "User mapping" code, or via MBT Applib PDUs
//    on the MBT Applib Interface: the <EPTF_MBT_Tester_CT> component has a <EPTF_MBT_PortType>
//    to control and observe the behavior of the <EPTF_MBT_LGen_CT> component running the FSM <f_EPTF_MBT_initLGenFsm> and
//    the applibs.
//    The MBT applib can be used to test other applibs from their FSM API point of view, or use applibs
//    as test harnesses.
//
//  Module paramters:
//    - tsp_EPTF_MBT_LGen_debug - *boolean* - Enable/disable verbose debugging
//    - tsp_EPTG_MBT_execute_lockTimeout - *float* - Maximum waiting time for
//      the f_EPTF_MBT_execute function when locking is used.
//
//  Module depends on:  
//    - <EPTF_CLL_Base_Definitions>
//    - <EPTF_CLL_Common_Definitions>
//    - <EPTF_CLL_Scheduler_Definitions>
//    - <EPTF_CLL_RBTScheduler_Definitions>
//    - <EPTF_CLL_LGenBase_Definitions>
//    - <EPTF_CLL_FBQ_Definitions>
//    - <EPTF_CLL_Logging_Definitions>
//////////////////////////////////////////////////// 
module EPTF_MBT_LGen_Definitions
{
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Scheduler_Definitions all;
import from EPTF_CLL_RBTScheduler_Definitions  all;
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_FBQ_Definitions all;
import from EPTF_CLL_Logging_Definitions all;
import from EPTF_CLL_Semaphore_Definitions all;

modulepar 
{
  boolean tsp_EPTF_MBT_LGen_debug := false;
  float tsp_EPTG_MBT_execute_lockTimeout := 5.0;
}

///////////////////////////////////////////////////////////////////////////////
//  const: c_EPTF_MBT_myBName
// 
//  Purpose:
//    constant for the behavior name
// 
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_MBT_myBName:="MBT_behavior";

//*******************************************************************************************************
//                                                 Events
//*******************************************************************************************************

///////////////////////////////////////////////////////////////////////////////
//  const: c_EPTF_MBT_inputIdx_incomingUserRequest
// 
//  Purpose:
//    constant for the input index of incomming user request
// 
///////////////////////////////////////////////////////////////////////////////
const integer    c_EPTF_MBT_inputIdx_incomingUserRequest := 0;

///////////////////////////////////////////////////////////////////////////////
//  const: c_EPTF_MBT_inputIdx_incomingUserRequest
// 
//  Purpose:
//    constant for the input name of incomming request
// 
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_MBT_inputName_incomingUserRequest := "EPTF_MBT_inputName_incomingUserRequest";

///////////////////////////////////////////////////////////////////////////////
//  const: c_EPTF_MBT_inputIdx_incomingTestStepRequest
// 
//  Purpose:
//    constant for the input index of incomming TesStep request
// 
///////////////////////////////////////////////////////////////////////////////
const integer    c_EPTF_MBT_inputIdx_incomingTestStepRequest := 1;

///////////////////////////////////////////////////////////////////////////////
//  const: c_EPTF_MBT_inputName_incomingTestStepRequest
// 
//  Purpose:
//    constant for the input name of TEstStep Request
// 
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_MBT_inputName_incomingTestStepRequest := "EPTF_MBT_inputName_incomingTestStepRequest";

const integer    c_EPTF_MBT_inputIdx_reportedVerdict := 2;
const charstring c_EPTF_MBT_inputName_reportedVerdict := "EPTF_MBT_inputName_reportedVerdict";

const integer c_EPTF_MBT_reportedVerdict_pass := 0;
const integer c_EPTF_MBT_reportedVerdict_fail := 1;
const integer c_EPTF_MBT_reportedVerdict_inconc := 2;



//*******************************************************************************************************
//                                                 TestSteps                    
//*******************************************************************************************************

///////////////////////////////////////////////////////////////////////////////
//  const: c_EPTF_MBT_stepIdx_invokeTestStep
//
//  Purpose:
//    constant for the step index of invoke TestStep
//
///////////////////////////////////////////////////////////////////////////////
const integer c_EPTF_MBT_stepIdx_invokeTestStep:=0;

///////////////////////////////////////////////////////////////////////////////
//  const: c_EPTF_MBT_stepIdx_invokeTestStep
//
//  Purpose:
//    constant for the step name of invoke TestStep
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_MBT_stepName_invokeTestStep :="EPTF_MBT_stepName_invokeTestStep";

///////////////////////////////////////////////////////////////////////////////
//  const: c_EPTF_MBT_stepIdx_sendUserResponse
//
//  Purpose:
//    constant for the step index of send user response
//
///////////////////////////////////////////////////////////////////////////////
const integer c_EPTF_MBT_stepIdx_sendUserResponse:=2;

///////////////////////////////////////////////////////////////////////////////
//  const: c_EPTF_MBT_stepName_sendUserResponse
//
//  Purpose:
//    constant for the step name of send user response
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_MBT_stepName_sendUserResponse :="EPTF_MBT_stepName_sendUserResponse"; 

///////////////////////////////////////////////////////////////////////////////
//  const: c_EPTF_MBT_stepIdx_invokeUserFunction
//
//  Purpose:
//    constant for the step index of invoke user function
//
///////////////////////////////////////////////////////////////////////////////
const integer c_EPTF_MBT_stepIdx_invokeUserFunction:=1;

///////////////////////////////////////////////////////////////////////////////
//  const: c_EPTF_MBT_stepName_invokeUserFunction
//
//  Purpose:
//    constant for the step name of invoke user function
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_MBT_stepName_invokeUserFunction :="EPTF_MBT_stepName_invokeUserFunction";

const integer c_EPTF_MBT_stepIdx_setMainFSM := 2;
const charstring c_EPTF_MBT_stepName_setMainFSM := "EPTF_MBT_stepName_setMainFSM";

const integer c_EPTF_MBT_stepIdx_reportVerdict := 3;
const charstring c_EPTF_MBT_stepName_reportVerdict := "EPTF_MBT_stepName_reportVerdict";


//*******************************************************************************************************
//                                                 Component Type                         
//*******************************************************************************************************

///////////////////////////////////////////////////////////////////////////////
// Type: EPTF_MBT_LGen_CT
//
// Purpose:
//   MBT application library component type
//
// Extensions:
//   EPTF_LGenBase_CT
//
//   EPTF_Scheduler_CT
//
//   EPTF_Logging_CT
//
// Variables:
//  v_EPTF_MBT_initialized - *boolean* - component initialization state
//
//  v_EPTF_MBT_myName - *charstring* - component's self name
//
//  vf_EPTF_MBT_entityGroupCreated - <fcb_EPTF_MBT_entityGroupCreated> - Entity Group created callback function
//
//  vf_EPTF_MBT_createUserResponse - <fcb_EPTF_MBT_customUserFunction> - Custom User function 
//      
//  v_EPTF_MBT_myBIdx - *integer* - self behavior index
//
//  v_EPTF_MBT_LoggingMaskId - *integer* - logging mask id
//
// Timers:
//   -
//
// Ports:
//   EPTF_MBT_PCO 				<EPTF_MBT_PortType>
//
// Detailed Comments:
//   -
//
///////////////////////////////////////////////////////////////////////////////
type component EPTF_MBT_LGen_CT extends EPTF_LGenBase_CT, EPTF_Scheduler_CT, EPTF_Logging_CT, EPTF_Semaphore_CT
{

  var boolean                         v_EPTF_MBT_initialized := false;
  var integer                         v_EPTF_MBT_myBIdx := -1;
  var charstring                      v_EPTF_MBT_myName := "";
  var integer                         v_EPTF_MBT_LoggingMaskId := -1;

  var EPTF_MBT_TestStepRequest        v_EPTF_MBT_TestStepRequest;
  var EPTF_MBT_EventDispatchRequest   v_EPTF_MBT_EventDispatchRequest;
  var EPTF_MBT_ConfigRequest          v_EPTF_MBT_ConfigRequest;
  var EPTF_MBT_CommandRequest         v_EPTF_MBT_CommandRequest;
  var EPTF_MBT_UserRequest            v_EPTF_MBT_UserRequest; 
  var EPTF_MBT_CreateFSMRequest       v_EPTF_MBT_CreateFSMRequest; 
  var EPTF_MBT_RemoveFSMRequest       v_EPTF_MBT_RemoveFSMRequest;

  var fcb_EPTF_MBT_entityGroupCreated vf_EPTF_MBT_entityGroupCreated := null;
  var fcb_EPTF_MBT_customUserFunction vf_EPTF_MBT_createUserResponse := null;

  port EPTF_MBT_PortType              EPTF_MBT_PCO;
}

///////////////////////////////////////////////////////////////////////////////
// Type: EPTF_MBT_Tester_CT
//
// Purpose:
//   MBT tester component type
//
// Extensions:
//   EPTF_LGenBase_CT
//
//   EPTF_Scheduler_CT
//
//   EPTF_Logging_CT
//
// Variables:
//   -
//
// Timers:
//   -
//
// Ports:
//   EPTF_MBT_PCO                     <EPTF_MBT_PortType>
//
// Detailed Comments:
//   -
//
///////////////////////////////////////////////////////////////////////////////
type component EPTF_MBT_Tester_CT extends EPTF_Base_CT, EPTF_Semaphore_CT
{
  port EPTF_MBT_PortType          EPTF_MBT_TESTER_PCO;
}

///////////////////////////////////////////////////////////////////////////////
// Type: EPTF_MBT_PortType
//
// Purpose:
//   Messages based port to connect the Tester comopnent to the loadGen component.
//
// Handled messages:   
//   inout EPTF_MBT_TestStepRequest,  EPTF_MBT_TestStepResponse;
//   inout EPTF_MBT_ConfigRequest,    EPTF_MBT_ConfigResponse;
//   inout EPTF_MBT_CommandRequest,   EPTF_MBT_CommandResponse;
//   inout EPTF_MBT_UserRequest,      EPTF_MBT_UserResponse;
//   inout EPTF_MBT_CreateFSMRequest, EPTF_MBT_CreateFSMResponse;
//   inout EPTF_MBT_RemoveFSMRequest, EPTF_MBT_RemoveFSMResponse;  
//
// Detailed Comments:
//   -
//
///////////////////////////////////////////////////////////////////////////////
type port EPTF_MBT_PortType message
{
  inout EPTF_MBT_TestStepRequest, EPTF_MBT_TestStepResponse;
  inout EPTF_MBT_ConfigRequest,   EPTF_MBT_ConfigResponse;
  inout EPTF_MBT_CommandRequest,  EPTF_MBT_CommandResponse;
  inout EPTF_MBT_UserRequest,     EPTF_MBT_UserResponse;
  inout EPTF_MBT_CreateFSMRequest,EPTF_MBT_CreateFSMResponse;
  inout EPTF_MBT_RemoveFSMRequest,EPTF_MBT_RemoveFSMResponse;
  inout EPTF_MBT_EventDispatchRequest;
}
with { extension "internal" }

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_TestStepRequest
// 
//  Purpose:
//    Describes the TestStep request
// 
//  Elements:
//    stepname - *charstring* - Name of the requested TestStep
//    stepArgs - <EPTF_IntegerList> - Container for the TestStep arguments
//    addr     - <FsmAddr>  - Address of the FSM
//    lock     - integer - After the test step was executed the semaphor with this
//               id will be unlocked by the applib.
//
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type record EPTF_MBT_TestStepRequest 
{
  charstring        stepName,
  EPTF_IntegerList  stepArgs,
  FsmAddr           addr optional,
  integer           lock optional
}

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_UserRequest
// 
//  Purpose:
//    Describes the User Request request
// 
//  Elements:
//    functionName - *charstring* - Name of the requested function
//    functionArgs - <EPTF_IntegerList> - Container for the function arguments
//    addr         - <FsmAddr>  - Address of the FSM
//
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type record EPTF_MBT_UserRequest 
{
  charstring        functionName,
  EPTF_IntegerList  functionArgs,
  FsmAddr           addr optional
}

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_UserResponse
// 
//  Purpose:
//    Describes the UserResponse 
// 
//  Elements:
//   -
//
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type record EPTF_MBT_UserResponse {};

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_CreateFSMRequest
// 
//  Purpose:
//    Describes the Create FSM request
// 
//  Elements:
//    entityGroupName - *charstring* - Name of the target entity group
//    eIdx - *integer* - Index of the target entity
//    fsmName - *charstring* - Name of the FSM to be activated
// 
///////////////////////////////////////////////////////////////////////////////
type record EPTF_MBT_CreateFSMRequest
{
  charstring  entityGroupName,
  integer     eIdx,
  charstring  fsmName
}

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_CreateFSMResponse
// 
//  Purpose:
//    Describes the Create FSM response
// 
//  Elements:
//    result - *integer* - The result (-1 in case of error)
//    addr - <FsmAddr> - The address of the activated FSM
// 
///////////////////////////////////////////////////////////////////////////////
type record EPTF_MBT_CreateFSMResponse
{
  integer   result,
  FsmAddr   addr
}

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_RemoveFSMRequest
// 
//  Purpose:
//    Describes the Remove FSM request
// 
//  Elements:
//    addr - <FsmAddr> - The address of the FSM to be removed
// 
///////////////////////////////////////////////////////////////////////////////
type record EPTF_MBT_RemoveFSMRequest
{
  FsmAddr   addr
}

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_CreateFSMResponse
// 
//  Purpose:
//    Describes the Remove FSM response
// 
//  Elements:
//    result - *integer* - The result (-1 in case of error)
// 
///////////////////////////////////////////////////////////////////////////////
type record EPTF_MBT_RemoveFSMResponse
{
  integer   result
}

///////////////////////////////////////////////////////////////////////////////
//  Type: FsmAddr
// 
//  Purpose:
//    Describes the FSM address
// 
//  Elements:
//    entityGroupName  - *charstring* - Name of the entity group
//    eIdx	       - *integer* - entity index 
//    fIdx 	       - *integer* - fsm index
//
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type record FsmAddr
{
  charstring  entityGroupName,
  integer     eIdx  optional,
  integer     fIdx  optional
}

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_TestStepResponse
// 
//  Purpose:
//   Describes the TestStep Response]
// 
//  Elements:
//   bName - *charstring* - Name of the behavior
//   iName - *charstring* - Name of the Event
//   addr - <FsmAddr> - Address of the FSM, where the event was reported.
//
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type record EPTF_MBT_TestStepResponse 
{
  charstring    bName,
  charstring    iName,
  FsmAddr       addr optional,
  EPTF_IntegerList  args optional
}

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_EventDispatchRequest
// 
//  Purpose:
//   Describes the Event Dispatch Request
// 
//  Elements:
//   bName - *charstring* - Name of the behavior
//   iName - *integer* - Index of the Event
//   addr - <FsmAddr> - Address of the FSM, where the event was reported.
//
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type record EPTF_MBT_EventDispatchRequest 
{
  charstring bName,
  integer    iName,
  FsmAddr    addr
}

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_ConfigRequest
// 
//  Purpose:
//   Describes the ConfigRequest
// 
//  Elements:
//   entityGroupName - *charstring*           - Name of the behavior
//   noEntities      - *integer*              - Number of entities within the group	
//   behaviors       - <EPTF_CharstringList>  - List of behavior that should be allocate to the group
//   fsmName         - *charstring*           - Name of the FSM to be assignedto teh entity group 
//
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type record EPTF_MBT_ConfigRequest
{
  charstring            entityGroupName,
  integer               noEntities,
  EPTF_CharstringList   behaviors,
  charstring            fsmName
}

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_ConfigResponse
// 
//  Purpose:
//   Describes the ConfigResponse
// 
//  Elements:
//   -
//
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type record EPTF_MBT_ConfigResponse {}

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_CommandRequest
// 
//  Purpose:
//   Describes the Command Request
// 
//  Elements:
//   quit comamnd that terminates the execution  
//
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type union EPTF_MBT_CommandRequest
{
  QuitCommand quit
}

///////////////////////////////////////////////////////////////////////////////
//  Type: EPTF_MBT_CommandResponse
// 
//  Purpose:
//   Describes the Command Response
// 
//  Elements:
//   - ready - <ReadyResponse> - Ready indication
//   - quitAck - <QuitAckResponse> - Ack for the quit command
//   
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type union EPTF_MBT_CommandResponse
{
  ReadyResponse   ready,
  QuitAckResponse quitAck
};

///////////////////////////////////////////////////////////////////////////////
//  Type: QuitCommand
// 
//  Purpose:
//   Describes the Command Response
// 
//  Elements:
//   - 
//   
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type record QuitCommand {}

///////////////////////////////////////////////////////////////////////////////
//  Type: ReadyResponse
// 
//  Purpose:
//   Describes the Ready Response
// 
//  Elements:
//   - 
//   
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type record ReadyResponse {}

///////////////////////////////////////////////////////////////////////////////
//  Type: QuitAckResponse
// 
//  Purpose:
//   Describes the Ready Response
// 
//  Elements:
//   - 
//   
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type record QuitAckResponse {}

///////////////////////////////////////////////////////////////////////////////
//  Type: fcb_EPTF_MBT_entityGroupCreated
// 
//  Purpose:
//   Describes the function signature for entity Group created callback function
// 
//  Elements:
//   p_req  - <EPTF_MBT_ConfigRequest> - describes the requetsed configuration
//   
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type function fcb_EPTF_MBT_entityGroupCreated(in EPTF_MBT_ConfigRequest p_req) runs on self;

///////////////////////////////////////////////////////////////////////////////
//  Type: fcb_EPTF_MBT_customUserFunction
// 
//  Purpose:
//   Describes the function signature for custom user function
// 
//  Elements:
//   pl_ptr  - <EPTF_LGenBase_TestStepArgs> - describes the requetsed configuration
//   
//  Detailed Comments:
//   -
// 
///////////////////////////////////////////////////////////////////////////////
type function fcb_EPTF_MBT_customUserFunction (in EPTF_LGenBase_TestStepArgs pl_ptr) runs on self return boolean;

}
